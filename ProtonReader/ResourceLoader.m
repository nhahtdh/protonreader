//
//  ResourceLoader.m
//  ProtonReader
//
//  Created by  on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ResourceLoader.h"
#import "SDURLCache.h"

#define PRErrorCodeFailToInitializeConnection -10

ResourceLoader* singleInstance;

@interface ResourceLoader ()

// @property (strong, nonatomic) NSMutableDictionary* runningRequests;
@property (strong, nonatomic) NSMutableDictionary* runningRequests;

- (void) asyncLoadResource:(NSURLRequest *)request delegate:(id)aDelegate completionHandler:(SEL)aCompletionHandler errorHandler:(SEL)aErrorHandler;

@end

@implementation ResourceLoader

@synthesize runningRequests;

- (id) init {
    if (self = [super init]) {
        // resources = [NSMutableArray array];
        runningRequests = [NSMutableDictionary dictionary];
    }
    
    return self;
}

+ (ResourceLoader*) sharedInstance {
    if (singleInstance) {
        return singleInstance;
    } else {
        return singleInstance = [[ResourceLoader alloc] init];
    }
}

+ (void) asyncLoadResource:(NSURLRequest *)request delegate:(id)aDelegate completionHandler:(SEL)aCompletionHandler errorHandler:(SEL)aErrorHandler {
    ResourceLoader* loader = [ResourceLoader sharedInstance];
    [loader asyncLoadResource: request delegate: aDelegate completionHandler: aCompletionHandler errorHandler: aErrorHandler];
}

- (void) asyncLoadResource:(NSURLRequest *)request delegate:(id)aDelegate completionHandler:(SEL)aCompletionHandler errorHandler:(SEL)aErrorHandler {
    
    DLog(@"asyncLoadResource for request: %@", request.URL);
    
    /*
    @synchronized (runningRequests) {
        resource = [runningRequests objectForKey: request];
        if (resource) {
            DLog(@"Found existing request in queue");
            // TODO: kResourceLoaded and kResourceLoadFailed
            if (resource.kResourceState == kResourceLoading) {
                DLog(@"Same request is being loaded.");
                [resource addObserver: observer];
                return;
            } else {
                DLog(@"/!\\WARNING: Unimplemented kResourceLoaded and kResourceLoadFailed");
                return;
            }
        } else {
            resource = [[Resource alloc] init];
            [resource addObserver: observer];
            [runningRequests setObject: resource forKey: request];
        }
    }
     */
    
    Resource* resource;
    ResourceObserver * resourceObserver = [ResourceObserver createObserver: aDelegate completionHandler: aCompletionHandler errorHandler: aErrorHandler];
    
    @synchronized (self.runningRequests) {
        resource = [self.runningRequests objectForKey: request];
        if (resource) {
            DLog(@"Found a running request.");
            [resource addObserver: resourceObserver];
            return;
        } else {
            DLog(@"Running request not found. Start a new request.");
            resource = [[Resource alloc] init];
            [resource addObserver: resourceObserver];
            [self.runningRequests setObject: resource forKey: request];
        }
    }
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest: request delegate: self];
    if (connection) {
        resource.data = [NSMutableData data];
    } else {
        @synchronized (self.runningRequests) {
            [self.runningRequests removeObjectForKey: request];
        }
        resource.error = [NSError errorWithDomain: @"ProtonLoader.ResourceLoader" code: PRErrorCodeFailToInitializeConnection userInfo: [NSDictionary dictionaryWithObjectsAndKeys: @"Description", @"Fail to initialize NSURLConnection", nil]];
        [resource notifyError];
    }
}

#pragma mark - NSURLConnectionDataDelegate

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    Resource* resource = [self.runningRequests objectForKey: connection.originalRequest];
    assert(resource);
        
    [(NSMutableData*) resource.data setLength: 0];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    Resource* resource = [self.runningRequests objectForKey: connection.originalRequest];
    assert(resource);
    
    [(NSMutableData*) resource.data appendData: data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    DLog(@"Download completed successfully from: %@", connection.originalRequest);
    Resource* resource;
    @synchronized (self.runningRequests) {
        resource = [self.runningRequests objectForKey: connection.originalRequest];
        assert(resource);
        [self.runningRequests removeObjectForKey: connection.originalRequest];
    }
    
    [resource notifyCompletion];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // NOTE: Can retry, but a bit tricky...
    DLog(@"Failed to load from network: %@", connection.originalRequest);
    DLog(@"Error - %@ %@", [error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    Resource* resource;
    @synchronized (self.runningRequests) {
        resource = [self.runningRequests objectForKey: connection.originalRequest];
        assert(resource);
        [self.runningRequests removeObjectForKey: connection.originalRequest];
    }
    
    NSCachedURLResponse *cachedResponse = [[SDURLCache sharedURLCache] cachedResponseForRequest:connection.originalRequest];
    if (cachedResponse) {
        DLog(@"Will use cached data for: %@", connection.originalRequest);
        resource.data = cachedResponse.data;
        
        [resource notifyCompletion];
    } else {
        DLog(@"Cached data not found for: %@", connection.originalRequest);
        resource.error = error;
        
        [resource notifyError];
    }
}

@end
