//
//  CommentsWidgetViewController.m
//  RenderTextDemo
//
//  Created by Shubham Goyal on 16/3/12.
//  Copyright (c) 2012 shubham.goyal@live.in. All rights reserved.
//

#import "CommentsWidgetViewController.h"

#define WIDTH_BACKGROUND 450
#define GAP_BETWEEN_BACKGROUND_AND_COMMENT_BOX 4
#define WIDTH_COMMENT_BOX (WIDTH_BACKGROUND - GAP_BETWEEN_BACKGROUND_AND_COMMENT_BOX)
#define WIDTH_SCREEN 1024
#define ANIMATION_DURATION 0.5
#define URL_PREFIX "http://ec2-175-41-180-218.ap-southeast-1.compute.amazonaws.com/article/list_comments/"

@interface CommentsWidgetViewController ()

@end

@implementation CommentsWidgetViewController

@synthesize commentsFeed;
@synthesize commentBox;
@synthesize comments;
@synthesize mainViewController;
@synthesize isClicked;
@synthesize commentsHeight;
@synthesize background;
@synthesize receivedData;

-(void) initializeComments {
    if (!comments) {
        self.comments = [[NSMutableArray alloc] init];
    }
}

-(id)initWithMainController:(ArticleViewController *)controller {
    if (self = [super init]) {
        self.mainViewController = controller;
        [self initializeComments];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:data];
}

/*- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    // inform the user
    DLog(@"Connection failed! Error - %@ %@",
         [error localizedDescription],
         [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    NSCachedURLResponse* cachedResponse = [[SDURLCache sharedURLCache] cachedResponseForRequest:connection.originalRequest];
    NSData *cachedData = [cachedResponse data];
    [self parsePublisherList:cachedData];
    activityIndicator.hidden = YES;
    [self updateView];
}*/

/*- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    [self parsePublisherList:receivedData];
    activityIndicator.hidden = YES;
    [self updateView];
    // release the connection, and the data object
}*/


-(void) loadCommentsInCommentBox {
    
    NSMutableString* url = [[NSMutableString alloc] initWithUTF8String:URL_PREFIX];
    NSString* suffixURL = [[NSString alloc] initWithFormat:@"%u", self.mainViewController.articleId];
    [url appendString:suffixURL];
    NSURL* URL = [[NSURL alloc] initWithString:url];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: URL cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval: 60. ];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (theConnection) {
        self.receivedData = [NSMutableData data];
    }
    
    NSArray *previousComments = [comments copy];
    self.comments = [[NSMutableArray alloc] init];
    self.commentsHeight = 0;
    for (int i = [previousComments count] - 1; i >= 0; i-- ) {
        id object = [previousComments objectAtIndex:i];
        if ([object isKindOfClass:[UITextView class]]) {
            [self addComment:((UITextView*)object).text];
            i--;
        }
    }
}

-(void) animateEntry {
    
    CGRect newFrameForBackground = CGRectMake(WIDTH_SCREEN - WIDTH_BACKGROUND, 0, self.background.frame.size.width, self.background.frame.size.height);
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         self.background.frame = newFrameForBackground;
                     }];
    
    /*CGRect newFrameForBox = CGRectMake(771, 596, 240, 86);
    CGRect newFrameForFeed = CGRectMake(771, 19, 240, 570);
    [UIView animateWithDuration:1
                     animations:^{
                         self.commentBox.frame = newFrameForBox;
                         self.commentsFeed.frame = newFrameForFeed;
                     }];*/
}

-(void) animateExit {
    
    CGRect newFrameForBackground = CGRectMake(1024, 0, self.background.frame.size.width, self.background.frame.size.height);
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         self.background.frame = newFrameForBackground;
                     }
                     completion:^(BOOL finished){
                         [self.background removeFromSuperview];
                         [self.commentBox removeFromSuperview];
                         [self.commentsFeed removeFromSuperview];
                         self.background = nil;
                         self.commentBox = nil;
                         self.commentsFeed = nil;
                     }];
    
    /*CGRect newFrameForBox = CGRectMake(1024, 596, 240, 86);
    CGRect newFrameForFeed = CGRectMake(1024, 19, 240, 570);
    [UIView animateWithDuration:1
                     animations:^{
                         self.commentBox.frame = newFrameForBox;
                         self.commentsFeed.frame = newFrameForFeed;
                     }
                     completion:^(BOOL finished){
                         [self.commentBox removeFromSuperview];
                         [self.commentsFeed removeFromSuperview];
                         self.commentBox = nil;
                         self.commentsFeed = nil;
                     }];*/
}

- (void) setFont:(UITextView*)textView {
    UIFont *textFont = [UIFont fontWithName:@"Georgia" size:16];
    textView.font = textFont;
}

-(void)createCommentBox {
    
    //self.background = [[UIView alloc] initWithFrame:CGRectMake(1024, 0, WIDTH_BACKGROUND, self.mainViewController.view.frame.size.width - self.mainViewController.bar.frame.size.height)];
    self.background = [[UIView alloc] initWithFrame:CGRectMake(1024, 0, WIDTH_BACKGROUND, self.mainViewController.view.frame.size.height - self.mainViewController.bar.frame.size.height)];
    DLog(@"%f", self.mainViewController.view.frame.size.width);
    self.background.backgroundColor = [UIColor whiteColor];
    self.commentBox = [[UITextView alloc] initWithFrame:CGRectMake(GAP_BETWEEN_BACKGROUND_AND_COMMENT_BOX, 596, WIDTH_COMMENT_BOX, 86)];
    [self setFont:self.commentBox];
    
    commentBox.editable = YES;
    self.commentBox.layer.borderWidth = 2.0f;
    self.commentBox.layer.borderColor = [[UIColor blackColor] CGColor];
    self.commentsFeed = [[UIScrollView alloc] initWithFrame:CGRectMake(GAP_BETWEEN_BACKGROUND_AND_COMMENT_BOX, 19, WIDTH_COMMENT_BOX, 570)];
    [self.mainViewController.view addSubview:self.background];
    [self.background addSubview:self.commentBox];
    [self.background addSubview:self.commentsFeed];
    [self loadCommentsInCommentBox];
    [self animateEntry];
    
    /*self.commentBox = [[UITextView alloc] initWithFrame:CGRectMake(1024, 596, 240, 86)];
    
    commentBox.editable = YES;
    self.commentBox.backgroundColor = [UIColor blueColor];
    
    self.commentsFeed = [[UIScrollView alloc] initWithFrame:CGRectMake(1024, 19, 240, 570)];
    
    [mainViewController.view addSubview:self.commentBox];
    [mainViewController.view addSubview:self.commentsFeed];
    
    [self loadCommentsInCommentBox];
    
    [self animateEntry];*/
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat keyboardOffset = keyboardSize.width;
    CGFloat totalOffset = self.mainViewController.bar.frame.size.height -keyboardOffset;
    
    CGRect newFrameForBackground = CGRectMake(self.background.frame.origin.x, self.background.frame.origin.y + totalOffset, self.background.frame.size.width, self.background.frame.size.height); 
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.background.frame = newFrameForBackground;
                     }];
    
    /*CGRect newFrameForBox = CGRectMake(commentBox.frame.origin.x, commentBox.frame.origin.y + totalOffset, commentBox.frame.size.width, commentBox.frame.size.height);
    CGRect newFrameForFeed = CGRectMake(commentsFeed.frame.origin.x, commentsFeed.frame.origin.y + totalOffset, commentsFeed.frame.size.width, commentsFeed.frame.size.height); 
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.commentBox.frame = newFrameForBox;
                         self.commentsFeed.frame = newFrameForFeed;
                     }];*/

}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat keyboardOffset = keyboardSize.width;
    CGFloat totalOffset = keyboardOffset - self.mainViewController.bar
    .frame.size.height;
    
    CGRect newFrameForBackground = CGRectMake(self.background.frame.origin.x, self.background.frame.origin.y + totalOffset, self.background.frame.size.width, self.background.frame.size.height); 
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.background.frame = newFrameForBackground;
                     }];
    
    /*CGRect newFrameForBox = CGRectMake(commentBox.frame.origin.x, commentBox.frame.origin.y + totalOffset, commentBox.frame.size.width, commentBox.frame.size.height);
    CGRect newFrameForFeed = CGRectMake(commentsFeed.frame.origin.x, commentsFeed.frame.origin.y + totalOffset, commentsFeed.frame.size.width, commentsFeed.frame.size.height);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.commentBox.frame = newFrameForBox;
                         self.commentsFeed.frame = newFrameForFeed;
                     }];*/
    
}

- (void) slideCommentsIn {
    
    [self createCommentBox];
    commentBox.delegate = self;
}

- (IBAction)buttonPressed:(id)sender {
    if (isClicked) {
        [self animateExit];
    }
    else {
        [self slideCommentsIn];
    }
    isClicked = !isClicked;
}

- (void)viewDidUnload
{
    [self setCommentsFeed:nil];
    [self setCommentBox:nil];
    self.comments = nil;
    self.mainViewController = nil;
    self.background = nil;
    self.receivedData = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
}

/*-(void) shiftCommentsDownBy:(CGFloat)height {
    
    for (int i = 2; i < [comments count]; i++) {
        UITextView *commentView = [comments objectAtIndex:i];
        commentView.frame = CGRectMake(commentView.frame.origin.x, commentView.frame.origin.y + height + 1, commentView.frame.size.width, commentView.frame.size.height);
    }
}*/

-(void) shiftCommentsUpBy:(CGFloat)height {
    for (int i = 0; i < [self.comments count] - 2; i++) {
        UIView *view = [self.comments objectAtIndex:i];
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - height, view.frame.size.width, view.frame.size.height);
    }
}

- (void) addComment:(NSString*)comment {
    
    if ((self.commentsFeed.contentSize.width == 0) && (self.commentsFeed.contentSize.height == 0)
        ) {
        self.commentsFeed.contentSize = CGSizeMake(WIDTH_COMMENT_BOX, 570);
    }
    
    UIView *upBorder = [[UIView alloc] initWithFrame:CGRectMake(0, commentsFeed.contentSize.height, commentBox.bounds.size.width, 1)];
    upBorder.backgroundColor = [UIColor grayColor];
    UITextView *commentView = [[UITextView alloc] initWithFrame:CGRectMake(0, commentsFeed.contentSize.height + upBorder.frame.size.height, commentBox.bounds.size.width, 100)];
    [self setFont:commentView];
    commentView.text = comment;
    commentView.editable = NO;
    
    [commentsFeed addSubview:commentView];
    
    CGFloat commentHeight = commentView.contentSize.height;
    
    [commentView removeFromSuperview];
    
    self.commentsHeight = self.commentsHeight + commentHeight + 1;
    
    [commentsFeed addSubview:upBorder];
    [commentsFeed addSubview:commentView];
    
    if (self.commentsHeight < self.commentsFeed.frame.size.height) {
        upBorder.frame = CGRectMake(0, self.commentsFeed.frame.size.height - commentHeight - 1, WIDTH_COMMENT_BOX, 1);
        commentView.frame = CGRectMake(0, upBorder.frame.origin.y + 1, WIDTH_COMMENT_BOX, commentHeight);
    }
    else {
        self.commentsFeed.contentSize = CGSizeMake(WIDTH_COMMENT_BOX, self.commentsHeight);
        upBorder.frame = CGRectMake(0, self.commentsHeight - commentHeight - 1, WIDTH_COMMENT_BOX, 1);
        commentView.frame = CGRectMake(0, upBorder.frame.origin.y + 1, WIDTH_COMMENT_BOX, commentHeight);
    }
    
    [comments insertObject:upBorder atIndex:[comments count]];
    [comments insertObject:commentView atIndex:[comments count]];
    
    if (self.commentsHeight < self.commentsFeed.frame.size.height) {
        [self shiftCommentsUpBy:commentHeight + 1];
    }
    
    /*UIView *upBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, commentBox.bounds.size.width, 1)];
    upBorder.backgroundColor = [UIColor grayColor];
    
    UITextView *commentView = [[UITextView alloc] initWithFrame:CGRectMake(0, 1, commentBox.bounds.size.width, 100)];
    commentView.text = comment;
    commentView.editable = NO;
    
    [commentsFeed addSubview:upBorder];
    [commentsFeed addSubview:commentView];
    
    commentsFeed.contentSize = CGSizeMake(commentsFeed.frame.size.width, commentsFeed.contentSize.height + commentView.contentSize.height + upBorder.frame.size.height);
    
    CGRect frame = commentView.frame;
    frame.size.height = commentView.contentSize.height;
    commentView.frame = frame;
    
    [comments insertObject:upBorder atIndex:0];
    [comments insertObject:commentView atIndex:0];
    [self shiftCommentsDownBy:frame.size.height];*/
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {  

    if ([text isEqualToString:@"\n"]) {  
        
        [textView resignFirstResponder];
        NSString *comment = commentBox.text;
        
        [self addComment:comment];
        commentBox.text = @"";
        
        return FALSE;
    }
    
    return TRUE;
}   


@end