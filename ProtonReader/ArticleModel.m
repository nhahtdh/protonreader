//
//  ArticleModel.m
//  ProtonReader
//
//  Created by  on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ArticleModel.h"

@implementation ArticleModel

@synthesize articleId;
@synthesize articleTitle;
@synthesize previewText;

#pragma mark - Custom initialization

#define kJSONArticleIdKey @"id"
#define kJSONArticleTitle @"title"
#define kJSONPreviewKey @"preview"

#define kDefaultTitle @"(No title)"
#define kDefaultPreview @""

- (id) initWithDictionary:(NSDictionary *)aDict {
    if (self = [super init]) {
        id value = [aDict valueForKey: kJSONArticleIdKey];
        assert(value && [value isKindOfClass: [NSNumber class]]);
        articleId =  [(NSNumber*) value unsignedIntegerValue];
        
        value = [aDict valueForKey: kJSONArticleTitle];
        if (value && [value isKindOfClass: [NSString class]]) {
            articleTitle = value;
        } else {
            articleTitle = kDefaultTitle;
        }
        
        value = [aDict valueForKey: kJSONPreviewKey];
        if (value && [value isKindOfClass: [NSString class]]) {
            previewText = value;
        } else {
            previewText = kDefaultPreview;
        }
    }
    
    return self;
}

@end
