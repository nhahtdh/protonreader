//
//  ArticleModel.h
//  ProtonReader
//
//  Created by  on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArticleModel : NSObject {
    NSUInteger articleId;
    NSString* articleTitle;
    NSString* previewText;
}

@property (nonatomic) NSUInteger articleId;
@property (strong, nonatomic) NSString *articleTitle;
@property (strong, nonatomic) NSString *previewText;

- (id) initWithDictionary: (NSDictionary*) aDict;

@end
