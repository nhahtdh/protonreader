//
//  CommentsWidgetViewController.h
//  RenderTextDemo
//
//  Created by Shubham Goyal on 16/3/12.
//  Copyright (c) 2012 shubham.goyal@live.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <QuartzCore/QuartzCore.h>
#import "ArticleViewController.h"
#import "SDURLCache.h"

@interface CommentsWidgetViewController : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) NSMutableArray *comments;
@property (weak) ArticleViewController *mainViewController;
@property (strong, nonatomic) UITextView *commentBox;
@property (strong, nonatomic) UIScrollView *commentsFeed;
@property BOOL isClicked;
@property CGFloat commentsHeight;
@property (strong) UIView *background;
@property (strong, nonatomic) NSMutableData* receivedData;

-(id) initWithMainController:(UIViewController*)controller;
-(IBAction)buttonPressed:(id)sender;

@end
