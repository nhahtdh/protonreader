//
//  LoaderDemoFirstViewController.h
//  ProtonReader
//
//  Created by  on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublisherModel.h"
#import "PublisherIconViewController.h"

@interface PublisherListingViewController : UIViewController <NSURLConnectionDataDelegate, UISearchBarDelegate> {
    NSMutableArray *publisherModels;
    
    NSURL *currentContent;
    NSMutableArray *publisherIcons;
    
    NSMutableData *receivedData;
}

@property (strong, nonatomic) NSMutableArray *publisherModels;

@property (strong, nonatomic) NSURL *currentContent;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)backButtonPressed:(id)sender;


@end
