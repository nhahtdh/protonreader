//
//  LoaderDemoSecondViewController.m
//  ProtonReader
//
//  Created by  on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SubscribedPublisherViewController.h"
#import "SubscriptionManager.h"
#import "SDURLCache.h"

#define kSubscribedPublishersFileName @"subscribed"

@interface SubscribedPublisherViewController () 

@property (strong) NSArray* publisherModels;
@property (strong, nonatomic) NSMutableArray *publisherIcons;

@property (strong, nonatomic) NSString* searchString;

- (void) updateView;

@end

@implementation SubscribedPublisherViewController

@synthesize publisherModels;
@synthesize publisherIcons;
@synthesize searchString;

@synthesize scrollView;
@synthesize searchBar;
@synthesize backButton;

#pragma mark - Custom initialization

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder: aDecoder]) {
        queue = [[NSOperationQueue alloc] init];
        publisherIcons = [NSMutableArray array];
    }
    
    return self;
}

#pragma mark - Memory warning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    DLog(@"/!\\ MEMORY WARNING!!!");
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

#define TOP_MARGIN 25
#define LEFT_MARGIN 40
#define RIGHT_MARGIN 40
#define Y_SEPARATION 20
#define ICONS_PER_ROW 5
#define ICONS_PER_COLUMN 5

- (void) updateView {
    for (PublisherIconViewController *icon in self.publisherIcons) {
        [icon.view removeFromSuperview];
        [icon removeFromParentViewController];
    }
    [self.publisherIcons removeAllObjects];
    
    int iconsPerScreen = ICONS_PER_ROW * ICONS_PER_COLUMN;
    int screenCount = self.publisherModels.count / iconsPerScreen + (self.publisherModels.count % iconsPerScreen != 0);
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width * screenCount, self.scrollView.bounds.size.height);
    
    CGPoint offset;
    
    int i = 0;
    for (PublisherModel* model in self.publisherModels) {
        if (self.searchString) {
            NSRange result = [model.name rangeOfString: self.searchString options: NSCaseInsensitiveSearch];
            if (result.location == NSNotFound)
                continue;
        }
        
        PublisherIconViewController* icon = [[UIStoryboard storyboardWithName: @"MainStoryboard" bundle: nil] instantiateViewControllerWithIdentifier: @"PublisherIconView"];
        icon.model = model;
        
        [self addChildViewController: icon];
        [self.publisherIcons addObject: icon];
        [self.scrollView addSubview: icon.view];
        
        if (i % iconsPerScreen == 0) {
            // TODO: Make multiple screens
            offset = CGPointMake(LEFT_MARGIN, TOP_MARGIN);
        }
        
        CGSize iconSize = icon.view.frame.size;
        CGFloat xSeparation = (self.view.frame.size.width - LEFT_MARGIN - RIGHT_MARGIN 
                               - ICONS_PER_ROW * iconSize.width) / (ICONS_PER_ROW - 1);
        
        icon.view.frame = CGRectMake(offset.x + (iconSize.width + xSeparation) * (i % ICONS_PER_ROW), offset.y + (iconSize.height + Y_SEPARATION) * (i / ICONS_PER_COLUMN), iconSize.width, iconSize.height);
        
        i++;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self updateView];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setBackButton:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    DLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    
    self.publisherModels = [[SubscriptionManager getInstance] getSubscribedPublisherModels];
    [self updateView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft;
}

- (IBAction)clearCache:(id)sender {
    [[SDURLCache sharedURLCache] removeAllCachedResponses];
}

#pragma mark - Search

-(void) searchBarSearchButtonClicked:(UISearchBar *) aSearchBar {
    assert(aSearchBar == self.searchBar);
    [aSearchBar resignFirstResponder];
    self.backButton.hidden = NO;
    
    self.searchString = aSearchBar.text;
    [self updateView];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.searchBar resignFirstResponder];
    self.backButton.hidden = YES;
    
    self.searchString = nil;
    [self updateView];
}

@end
