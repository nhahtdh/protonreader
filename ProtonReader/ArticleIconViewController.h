//
//  ArticleIconViewController.h
//  ProtonReader
//
//  Created by  on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleModel.h"
#import "ArticleViewController.h"

@interface ArticleIconViewController : UIViewController {
    ArticleModel* model;
}

@property (strong, nonatomic) ArticleModel* model;

- (void) updateView;

#pragma mark - Outlet

@property (weak, nonatomic) IBOutlet UILabel *titleView;
@property (weak, nonatomic) IBOutlet UILabel *previewTextView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
