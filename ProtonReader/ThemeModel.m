//
//  ThemeModel.m
//  ProtonReader
//
//  Created by  on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ThemeModel.h"

@implementation ThemeModel 

@synthesize primaryHeader;
@synthesize secondaryHeader;

#pragma mask - Custom initialization

#define kJSONPrimaryHeaderKey @"primaryHeader"
#define kJSONSecondaryHeaderKey @"secondaryHeader"

- (id) initWithDictionary: (NSDictionary*) aDict {
    if (self = [super init]) {
        id value = [aDict objectForKey: kJSONPrimaryHeaderKey];
        if (value && [value isKindOfClass: [NSString class]]) {
            primaryHeader = [[UIImage alloc] initWithData: [[NSData alloc] initWithContentsOfURL: [[NSURL alloc] initWithString: value]]];
        } else {
            primaryHeader = nil;
        }
        
        value = [aDict objectForKey: kJSONSecondaryHeaderKey];
        if (value && [value isKindOfClass: [NSString class]]) {
            secondaryHeader = [[UIImage alloc] initWithData: [[NSData alloc] initWithContentsOfURL: [[NSURL alloc] initWithString: value]]];
        } else {
            secondaryHeader = nil;
        }
    }
    
    return self;
}

@end
