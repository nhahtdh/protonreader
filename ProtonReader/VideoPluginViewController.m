//
//  VideoPluginViewController.m
//  RenderTextDemo
//
//  Created by Shubham Goyal on 21/3/12.
//  Copyright (c) 2012 shubham.goyal@live.in. All rights reserved.
//

#import "VideoPluginViewController.h"

@interface VideoPluginViewController ()

@end

@implementation VideoPluginViewController
@synthesize video;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSString* urlString = @"http://www.youtube.com/watch?v=f60dheI4ARg";
    NSString *youTubeVideoHTML = @"<html><head></head><body style=\"margin:0;\"><embed src=\"%@\" type=\"application/x-shockwave-flash\" width=\"%f\" height=\"%f\"></embed></body></html>";
    NSString *html = [NSString stringWithFormat:youTubeVideoHTML, urlString, video.frame.size.width, video.frame.size.height];
    [video loadHTMLString:html baseURL:nil];

}

- (void)viewDidUnload
{
    [self setVideo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
