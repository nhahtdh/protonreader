//
//  SubscriptionManager.m
//  ProtonReader
//
//  Created by  on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SubscriptionManager.h"

SubscriptionManager* singleInstance;

@interface SubscriptionManager ()

@property (strong) NSMutableArray* subscribedPublishers;

#pragma mark - Persistence

- (void) loadSubscribedPublishers;

- (void) saveSubscribedPublishers;

@end

@implementation SubscriptionManager

@synthesize subscribedPublishers;

- (id) init {
    if (self = [super init]) {
        [self loadSubscribedPublishers];
    }
    
    return self;
}

+ (SubscriptionManager*) getInstance {
    if (!singleInstance) {
        singleInstance = [[self alloc] init];
    }
    
    return singleInstance;
}

#pragma mark - Model

- (NSArray*) getSubscribedPublisherModels {
    return [NSArray arrayWithArray: self.subscribedPublishers];
}

- (BOOL) isSubscribedTo:(PublisherModel *)model {
    return [self.subscribedPublishers containsObject: model];
}

// TODO: Update the publisher models to the latest version by querying server

- (void) subscribeToPublisher:(PublisherModel *)model {
    if ([self.subscribedPublishers containsObject: model]) {
        DLog(@"/!\\ WARNING: The same publisher should not be subscribed twice!");
    } else {
        [self.subscribedPublishers addObject: model];
        [self performSelectorInBackground: @selector(saveSubscribedPublishers) withObject:nil];
    }
}

- (void) unsubscribeToPublisher:(PublisherModel *)model {
    if (![self.subscribedPublishers containsObject: model]) {
        DLog(@"/!\\ WARNING: Unsubscribe an unknown publisher");
    } else {
        [self.subscribedPublishers removeObject: model];
        [self performSelectorInBackground: @selector(saveSubscribedPublishers) withObject:nil];
    }
}

#pragma mark - Persistence

#define kDataKey @"data"
#define kFileName @"subscribed.plist"

- (void) loadSubscribedPublishers {
    DLog(@"Loading subscribed publishers");
    NSArray* dirPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDirectory, YES);
    assert(dirPaths != nil);
    assert(dirPaths.count > 0);
    NSString* libDir = [dirPaths objectAtIndex: 0];
    NSString *filePath = [libDir stringByAppendingPathComponent: kFileName];
    
    NSData *data = [[NSData alloc] initWithContentsOfFile: filePath];
    if (!data) {
        DLog(@"%@ does not exist or cannot be read from", filePath);
        self.subscribedPublishers = [NSMutableArray array];
        return;
    }
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData: data];
    self.subscribedPublishers = [[unarchiver decodeObjectForKey: kDataKey] mutableCopy];
    assert(self.subscribedPublishers);
    [unarchiver finishDecoding];
}

- (void) saveSubscribedPublishers {
    DLog(@"Saving subscribed publishers");
    NSArray* dirPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDirectory, YES);
    assert(dirPaths != nil);
    assert(dirPaths.count > 0);
    NSString* libDir = [dirPaths objectAtIndex: 0];
    NSString *filePath = [libDir stringByAppendingPathComponent: kFileName];
    
    NSMutableData *data = [NSMutableData alloc];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject: self.subscribedPublishers forKey: kDataKey];
    [archiver finishEncoding];
    NSError *error;
    [data writeToFile: filePath options: NSDataWritingAtomic error: &error];
    if (error) {
        DLog(@"Error occurred while writing to file %@", filePath);
        DLog(@"Debug description: %@", error.debugDescription);
        assert(0);
    }
}

@end
