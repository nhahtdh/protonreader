//
//  ResourceObserver.h
//  ProtonReader
//
//  Created by  on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResourceObserver: NSObject {
    id target;
    SEL completionHandler; // void(^)(NSData*)
    SEL errorHandler; // void(^)(NSError*)
}

#pragma mark - Initialization

+ (ResourceObserver*) createObserver: (id)aTarget completionHandler: (SEL) aCompletionHandler errorHandler: (SEL) aErrorHandler;

#pragma mark - Notification

- (void) performCompletionHandlerWithData: (NSData*) data;
// NOTE: The handler will be performed on main thread

- (void) performErrorHandlerWithError: (NSError*) error;
// NOTE: The handler will be performed on main thread

@end
