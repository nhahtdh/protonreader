//
//  PublisherIconViewController.m
//  ProtonReader
//
//  Created by  on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PublisherIconViewController.h"
#import "ProviderScreenViewController.h"

@implementation PublisherIconViewController

@synthesize model;

@synthesize icon;
@synthesize name;

#pragma mark - Memory warning

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) updateView {
    DLog(@"%@ updateView called", self);
    if (self.model) {
        self.icon.image = [UIImage imageWithData: self.model.logo];
        self.icon.layer.cornerRadius = 10;
        
        self.name.text = model.name;
    } else {
        self.icon.image = nil;
        
        self.name.text = @"";
    }
    // [self.view setNeedsDisplay];
}

- (void)viewDidLoad
{
    DLog(@"%@ viewDidLoad called", self);
    [super viewDidLoad];
    
    [self updateView];
    
    DLog(@"%@ viewDidLoad done", self);
}

- (void)viewDidUnload
{
    [self setIcon:nil];
    [self setName:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    DLog(@"viewDidAppear");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Segue

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"OpenProviderScreen"]) {
        assert([segue.destinationViewController isKindOfClass:[ProviderScreenViewController class]]);
        ProviderScreenViewController *dest = segue.destinationViewController;
        dest.publisherModel = self.model;
    }
}

@end
