//
//  ViewController.m
//  RenderTextDemo
//
//  Created by Shubham Goyal on 10/3/12.
//  Copyright (c) 2012 shubham.goyal@live.in. All rights reserved.
//

#import "ArticleViewController.h"
#import "CommentsWidgetViewController.h"
#import "RatingWidgetViewController.h"
#import "SDURLCache.h"

#define BAR_HEIGHT 60
#define IPAD_HEIGHT 748
#define IPAD_WIDTH 1024
#define ANIMATION_DURATION 0.5
#define URL_PREFIX "http://ec2-175-41-180-218.ap-southeast-1.compute.amazonaws.com/article/list_article/"

@implementation ArticleViewController

@synthesize comments;
@synthesize ratingButton;
@synthesize commentsButton;
@synthesize bar;
@synthesize isRatingButtonClicked;
@synthesize back;
@synthesize next;
@synthesize rating;
@synthesize pages;
@synthesize pageNumber = _pageNumber;
@synthesize pageViews;
@synthesize queue;
@synthesize requestLock;
@synthesize articleId;

- (void) loadArticles {
    NSMutableString* url = [[NSMutableString alloc] initWithUTF8String:URL_PREFIX];
    NSString* suffixURL = [[NSString alloc] initWithFormat:@"%u", self.articleId];
    [url appendString:suffixURL];
    NSURL* URL = [[NSURL alloc] initWithString:url];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: URL];
    
    NSURLResponse *response;
    NSError *error;
    DLog(@"Send request ...")
    NSData *data = [NSURLConnection sendSynchronousRequest: request returningResponse: &response error: &error];
    
    if (error) {
        DLog(@"Error happened");
        assert(0);
    }
    
    DLog(@"Normal? Hope so...");
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData: data options: kNilOptions error: nil];
    DLog(@"%@", json.description);
    NSArray *data_ = [json objectForKey: @"data"];
    DLog(@"%@", data_.description);
    NSDictionary *obj = [data_ objectAtIndex: 0];
    DLog(@"%@", obj.description);
    NSArray *content = [obj objectForKey: @"content"];
    DLog(@"%@", content.description);
    
    for (NSDictionary* dict in content) {
        DLog(@"%@", [dict objectForKey: @"content"]);
        [self.pages addObject: [dict objectForKey: @"content"]];
    }
    
    DLog(@"OK!");
    
    self.pageNumber = 0;
    
    int i = 0;
    for (NSString* pageContent in self.pages) {
        UIWebView* pageView = [[UIWebView alloc] initWithFrame:CGRectMake(i * IPAD_WIDTH, 0, IPAD_WIDTH, IPAD_HEIGHT - BAR_HEIGHT)];
        [pageView loadHTMLString: pageContent baseURL:nil];
        pageView.scrollView.scrollEnabled = NO;
        [self.pageViews addObject:pageView];
        [self.view addSubview:pageView];
        
        UISwipeGestureRecognizer *leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftSwipeGesture:)];
        leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
        [pageView addGestureRecognizer:leftSwipeGestureRecognizer];
        
        UISwipeGestureRecognizer *rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightSwipeGesture:)];
        rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
        [pageView addGestureRecognizer:rightSwipeGestureRecognizer];
        
        i++;
    }
}

- (void) handleLeftSwipeGesture:(UISwipeGestureRecognizer*)swipeGestureRecognizer {
    [self nextButtonPressed:nil];
}

- (void) handleRightSwipeGesture:(UISwipeGestureRecognizer*)swipeGestureRecognizer {
    [self backButtonPressed:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.pages = [[NSMutableArray alloc] init];
    self.pageViews = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeButtonStatuses) name:@"pageNumberSet" object:nil];
    
    [self loadArticles];
    self.comments = [[CommentsWidgetViewController alloc] initWithMainController:self];
    self.rating = [[RatingWidgetViewController alloc] initWithMainController:self];
    [self.comments viewDidLoad];
}

- (void) changeButtonStatuses {
    if (self.pageNumber == 0)
        self.back.hidden = YES;
    else
        self.back.hidden = NO;
    if (self.pageNumber == ([self.pages count] - 1))
        self.next.hidden = YES;
    else 
        self.next.hidden = NO;
}

- (void)setPageNumber:(int)pageNumber {
    _pageNumber = pageNumber;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pageNumberSet" object:@"nil"];
}

- (void)viewDidUnload
{    
    [self setRating:nil];
    [self setComments:nil];
    [self setRatingButton:nil];
    [self setCommentsButton:nil];
    [self setBar:nil];
    self.comments = nil;
    [self setBack:nil];
    [self setNext:nil];
    self.pages = nil;
    self.pageViews = nil;
    self.queue = nil;
    [super viewDidUnload];    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (IBAction)backButtonPressed:(id)sender {
    if (self.pageNumber == 0) {
        DLog(@"Sorry, cannot move back because this is the first page");
        return;
    }
    else {
        int *newCenters = malloc([self.pages count] * sizeof(int));
        for (int i = 0; i < [self.pages count]; i++) {
            CGPoint oldCenter = [[self.pageViews objectAtIndex:i] center];
            int newCenter = oldCenter.x + IPAD_WIDTH;
            newCenters[i] = newCenter;
        }
        
        [UIView animateWithDuration:ANIMATION_DURATION
                         animations:^{
                             for (int i = 0; i < [self.pages count]; i++) {
                                 UIWebView *webView = [self.pageViews objectAtIndex:i];
                                 webView.center = CGPointMake(newCenters[i], webView.center.y);
                             }
                         }];
        
    }
    
    self.pageNumber -= 1;
}

- (IBAction)nextButtonPressed:(id)sender {
    if (self.pageNumber == ([self.pageViews count] - 1)) {
        DLog(@"Sorry, cannot move to the next page because this is the last page");
        return;
    }
    else {
        int *newCenters = malloc([self.pages count] * sizeof(int));
        for (int i = 0; i < [self.pages count]; i++) {
            CGPoint oldCenter = [[self.pageViews objectAtIndex:i] center];
            int newCenter = oldCenter.x - IPAD_WIDTH;
            newCenters[i] = newCenter;
        }
        
        [UIView animateWithDuration:ANIMATION_DURATION
                         animations:^{
                             for (int i = 0; i < [self.pages count]; i++) {
                                 UIWebView *webView = [self.pageViews objectAtIndex:i];
                                 webView.center = CGPointMake(newCenters[i], webView.center.y);
                             }
                         }];
        
        
    }
    
    self.pageNumber += 1;
}

- (IBAction)ratingButtonPressed:(id)sender {
    if (isRatingButtonClicked) {
        [self.rating viewDidUnload];
    }
    else {
        if (self.rating.rateView != nil) {
            [self.rating.rateView removeFromSuperview];
            self.rating.rateView = nil;
        }
        [self.rating viewDidLoad];
    }
    self.isRatingButtonClicked = !self.isRatingButtonClicked;
}

- (IBAction)commentsButtonPressed:(id)sender {
    [self.comments buttonPressed:sender];
}
@end
