//
//  LoaderDemoFirstViewController.m
//  ProtonReader
//
//  Created by  on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PublisherListingViewController.h"
#import "SDURLCache.h"

@interface PublisherListingViewController ()

@property (strong, nonatomic) NSMutableArray* publisherIcons;

- (BOOL) isFatalError: (NSError*) error;
- (void) loadPublisherList;
- (void) parsePublisherList: (NSData*) data;

- (void) updateView;

@end

#define kPublisherListingURL @"http://ec2-175-41-180-218.ap-southeast-1.compute.amazonaws.com/publisher/list_publisher/all"
#define kSearchPublisherListingURLFormat @"http://ec2-175-41-180-218.ap-southeast-1.compute.amazonaws.com/publisher/search/%@"

@implementation PublisherListingViewController

@synthesize publisherModels;

@synthesize currentContent;
@synthesize publisherIcons;

@synthesize activityIndicator;
@synthesize scrollView;
@synthesize searchBar;
@synthesize backButton;

#pragma mark - Custom initialization

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder: aDecoder]) {
        // TODO: Specify capacity later
        publisherModels = [NSMutableArray array]; 
        publisherIcons = [NSMutableArray array];
        
        currentContent = [NSURL URLWithString: kPublisherListingURL];
    }
    
    return self;
}

#pragma mark - Memory warning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DLog(@"/!\\ MEMORY WARNING!!!");
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Network and data

- (BOOL) isFatalError: (NSError*) error {
    switch (error.code) {
        case NSURLErrorBadURL:
        case NSURLErrorBadServerResponse:
        case NSURLErrorRedirectToNonExistentLocation:
        case NSURLErrorFileDoesNotExist:
        case NSURLErrorFileIsDirectory:
        case NSURLErrorUnsupportedURL:
            DLog(@"Fatal error. Error code: %d. Debug description: %@", error.code, error.debugDescription);
            return YES;
            
        default:
            DLog(@"Error occurred. Error code: %d. Debug description: %@", error.code, error.debugDescription);
            return NO;
    }
}

#define kJSONSuccessKey @"success"
#define kJSONErrorMessageKey @"error_msg"
#define kJSONPublisherListKey @"data"

- (void) parsePublisherList: (NSData*) data {
    NSError* jsonParsingError = nil;
    NSDictionary* responseJSON = [NSJSONSerialization JSONObjectWithData: data options: kNilOptions error: &jsonParsingError];
    
    assert(responseJSON == nil ^ jsonParsingError == nil);
    if (jsonParsingError) {
        DLog(@"JSON parsing error: %@", jsonParsingError.debugDescription);
        DLog(@"Data dump: %@",  [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]);
        return;
    }
    assert([responseJSON isKindOfClass: [NSDictionary class]]);
    
    BOOL isSuccess = [(NSNumber*) [responseJSON valueForKey: kJSONSuccessKey] boolValue];
    if (!isSuccess) {
        DLog(@"Error encountered on server: %@", [responseJSON valueForKey: kJSONErrorMessageKey]);
        DLog(@"Data dump: %@",  [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]);
        return;
    }
    
    NSArray* publisherList = [responseJSON valueForKey: kJSONPublisherListKey];
    assert(publisherList && [publisherList isKindOfClass: [NSArray class]]);
    DLog(@"Number of publishers in JSON: %d", publisherList.count);
    [self.publisherModels removeAllObjects];
    for (NSDictionary* publisher in publisherList) {
        PublisherModel *model = [[PublisherModel alloc] initWithDictionary: publisher];
        [self.publisherModels addObject: model];
    }
    DLog(@"Number of publisher models: %d", self.publisherModels.count);
}

- (void) loadPublisherList {
    self.scrollView.hidden = YES;
    [self.activityIndicator startAnimating];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: self.currentContent cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval: 60.];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection) {
        receivedData = [NSMutableData data];
    } else {
        DLog(@"Failed to initialize NSURLConnection. Try to load from cache.");
        
        NSCachedURLResponse* cachedResponse = [[SDURLCache sharedURLCache] cachedResponseForRequest:connection.originalRequest];
        NSData *cachedData = [cachedResponse data];
        
        [self parsePublisherList: cachedData];
        
        // Update UI
        [self updateView];
    }
}

#define TOP_MARGIN 25
#define LEFT_MARGIN 40
#define RIGHT_MARGIN 40
#define Y_SEPARATION 20
#define ICONS_PER_ROW 5
#define ICONS_PER_COLUMN 5

- (void) updateView {
    for (PublisherIconViewController *icon in self.publisherIcons) {
        [icon.view removeFromSuperview];
        [icon removeFromParentViewController];
    }
    [self.publisherIcons removeAllObjects];
    
    int iconsPerScreen = ICONS_PER_ROW * ICONS_PER_COLUMN;
    int screenCount = self.publisherModels.count / iconsPerScreen + (self.publisherModels.count % iconsPerScreen != 0);
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width * screenCount, self.scrollView.bounds.size.height);
    
    CGPoint offset;
    
    int i = 0;
    for (PublisherModel* model in self.publisherModels) {
        PublisherIconViewController* icon = [[UIStoryboard storyboardWithName: @"MainStoryboard" bundle: nil] instantiateViewControllerWithIdentifier: @"PublisherIconView"];
        icon.model = model;
        
        [self addChildViewController: icon];
        [self.publisherIcons addObject: icon];
        [self.scrollView addSubview: icon.view];
        
        if (i % iconsPerScreen == 0) {
            // TODO: Make multiple screens
            offset = CGPointMake(LEFT_MARGIN, TOP_MARGIN);
        }
        
        CGSize iconSize = icon.view.frame.size;
        CGFloat xSeparation = (self.view.frame.size.width - LEFT_MARGIN - RIGHT_MARGIN 
                               - ICONS_PER_ROW * iconSize.width) / (ICONS_PER_ROW - 1);
        
        icon.view.frame = CGRectMake(offset.x + (iconSize.width + xSeparation) * (i % ICONS_PER_ROW), offset.y + (iconSize.height + Y_SEPARATION) * (i / ICONS_PER_COLUMN), iconSize.width, iconSize.height);
        
        i++;
    }
    
    self.scrollView.hidden = NO;
    [activityIndicator stopAnimating];
}

#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self parsePublisherList: receivedData];
    
    // Update UI
    [self updateView];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    DLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    NSCachedURLResponse* cachedResponse = [[SDURLCache sharedURLCache] cachedResponseForRequest:connection.originalRequest];
    NSData *cachedData = [cachedResponse data];
    
    [self parsePublisherList: cachedData];
    
    [self updateView];
}

#pragma mark - Searching

- (void) searchBarSearchButtonClicked:(UISearchBar *)aSearchBar {
    assert(aSearchBar == self.searchBar);
    [aSearchBar resignFirstResponder];
    
    self.backButton.hidden = NO;
    self.currentContent = [NSURL URLWithString: [NSString stringWithFormat: kSearchPublisherListingURLFormat, aSearchBar.text]];
    
    [self loadPublisherList];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.searchBar resignFirstResponder];
    
    self.backButton.hidden = YES;
    self.currentContent = [NSURL URLWithString: kPublisherListingURL];
    
    [self loadPublisherList];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)viewDidUnload
{
    [self setActivityIndicator:nil];
    [self setScrollView:nil];
    [self setBackButton:nil];
    [self setSearchBar: nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    DLog(@"viewWillAppear");
    [super viewWillAppear:animated];
    
    [self loadPublisherList];
    
    DLog(@"Number of subviews in scroll view: %d", self.scrollView.subviews.count);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

@end
