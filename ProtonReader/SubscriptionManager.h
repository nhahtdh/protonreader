//
//  SubscriptionManager.h
//  ProtonReader
//
//  Created by  on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PublisherModel.h"

@interface SubscriptionManager : NSObject {
    NSMutableArray *subscribedPublishers;
}

#pragma mark - Model

+ (SubscriptionManager*) getInstance;

- (NSArray*) getSubscribedPublisherModels;

- (BOOL) isSubscribedTo: (PublisherModel *)model;

- (void) subscribeToPublisher: (PublisherModel *)model;

- (void) unsubscribeToPublisher: (PublisherModel *)model;

@end
