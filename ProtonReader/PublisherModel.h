//
//  PublisherModel.h
//  ProtonReader
//
//  Created by  on 3/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PublisherModel : NSObject <NSCoding> {
    NSUInteger publisherId;
    NSString *name;
    NSData *logo;
    NSString *summary;
}

#pragma mark - Properties

@property (nonatomic, readwrite) NSUInteger publisherId;
@property (strong, nonatomic, readwrite) NSString *name;
@property (strong, nonatomic, readwrite) NSData *logo;
@property (strong, nonatomic, readwrite) NSString* summary;

#pragma mark - Custom initialization

- (id) initWithCoder: (NSCoder*) aDecoder;
- (id) initWithDictionary: (NSDictionary*) aDict;

#pragma mark - Persistence

- (void) encodeWithCoder:(NSCoder *)aCoder;

#pragma mark - Object identity

- (BOOL) isEqual:(id)object;
- (NSUInteger)hash;

@end
