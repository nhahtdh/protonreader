//
//  ViewController.h
//  RenderTextDemo
//
//  Created by Shubham Goyal on 10/3/12.
//  Copyright (c) 2012 shubham.goyal@live.in. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VideoPluginViewController.h"

@class CommentsWidgetViewController;
@class RatingWidgetViewController;

@interface ArticleViewController : UIViewController

@property (strong) CommentsWidgetViewController* comments;
@property (strong) RatingWidgetViewController* rating;
@property (strong, nonatomic) IBOutlet UIButton *ratingButton;
@property (strong, nonatomic) IBOutlet UIButton *commentsButton;
@property (strong, nonatomic) IBOutlet UIView *bar;
@property BOOL isRatingButtonClicked;
@property (strong, nonatomic) IBOutlet UIButton *back;
@property (strong, nonatomic) IBOutlet UIButton *next;
@property (strong) NSMutableArray *pages;
@property (strong) NSMutableArray *pageViews;
@property (nonatomic) int pageNumber;

@property (nonatomic) NSUInteger articleId;
@property (strong) NSOperationQueue *queue;
@property BOOL requestLock;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)nextButtonPressed:(id)sender;
- (IBAction)ratingButtonPressed:(id)sender;
- (IBAction)commentsButtonPressed:(id)sender;

@end
