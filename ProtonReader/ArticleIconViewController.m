//
//  ArticleIconViewController.m
//  ProtonReader
//
//  Created by  on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ArticleIconViewController.h"

@implementation ArticleIconViewController

@synthesize model;

@synthesize titleView;
@synthesize previewTextView;
@synthesize imageView;

#pragma mark - Memory warning

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) updateView {
    self.titleView.text = self.model.articleTitle;
    // self.previewTextView.text = self.model.previewText;
    
    [self.view setNeedsDisplay];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateView];
}

- (void)viewDidUnload
{
    [self setTitleView:nil];
    [self setPreviewTextView:nil];
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Segue

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString: @"OpenArticleScreen"]) {
        assert([segue.destinationViewController isKindOfClass: [ArticleViewController class]]);
        ArticleViewController *dest = segue.destinationViewController;
        dest.articleId = self.model.articleId;
    }
}

@end
