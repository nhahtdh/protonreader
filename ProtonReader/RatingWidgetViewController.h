//
//  RatingWidgetViewController.h
//  RenderTextDemo
//
//  Created by Shubham Goyal on 27/3/12.
//  Copyright (c) 2012 shubham.goyal@live.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
#import "ArticleViewController.h"

@interface RatingWidgetViewController : UIViewController
@property (strong, nonatomic) RateView *rateView;
@property (strong) ArticleViewController* mainViewController;
@property float rating;

-(id)initWithMainController:(ArticleViewController*)controller;

@end
