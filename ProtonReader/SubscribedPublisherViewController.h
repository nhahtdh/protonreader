//
//  LoaderDemoSecondViewController.h
//  ProtonReader
//
//  Created by  on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublisherIconViewController.h"
#import "PublisherModel.h"

@interface SubscribedPublisherViewController : UIViewController {
    NSOperationQueue *queue;
    
    NSArray* publisherModels;
    NSMutableArray* publisherIcons;
    
    NSString *searchString;
}

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)backButtonPressed:(id)sender;


// TODO: Remove this function. This is a function to test the cache only.
- (IBAction)clearCache:(id)sender;

@end
