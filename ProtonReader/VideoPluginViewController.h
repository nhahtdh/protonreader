//
//  VideoPluginViewController.h
//  RenderTextDemo
//
//  Created by Shubham Goyal on 21/3/12.
//  Copyright (c) 2012 shubham.goyal@live.in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoPluginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *video;

@end
