//
//  ResourceObserver.m
//  ProtonReader
//
//  Created by  on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ResourceObserver.h"

@interface ResourceObserver ()

- (id) initWithTarget: (id)aTarget completionHandler:(SEL)aCompletionHandler errorHandler:(SEL)aErrorHandler;

@end

@implementation ResourceObserver

#pragma mark - Initialization

- (id) initWithTarget: (id)aTarget completionHandler:(SEL)aCompletionHandler errorHandler:(SEL)aErrorHandler {
    if (self = [super init]) {
        target = aTarget;
        completionHandler = aCompletionHandler;
        errorHandler = aErrorHandler;
    }
    
    return self;
}

+ (ResourceObserver *)createObserver:(id)aTarget completionHandler:(SEL)aCompletionHandler errorHandler:(SEL)aErrorHandler {
    if (aTarget) {
        assert(!aCompletionHandler || [aTarget respondsToSelector: aCompletionHandler]);
        assert(!aErrorHandler || [aTarget respondsToSelector: aErrorHandler]);
        return [[ResourceObserver alloc] initWithTarget: aTarget completionHandler: aCompletionHandler errorHandler: aErrorHandler];
    } else {
        return nil;
    }
}

#pragma mark - Notification

- (void)performCompletionHandlerWithData:(NSData *)data {
    if (completionHandler) 
        [target performSelectorOnMainThread: completionHandler withObject: data waitUntilDone: NO];
}


- (void)performErrorHandlerWithError:(NSError *)error {
    if (errorHandler)
        [target performSelectorOnMainThread: errorHandler withObject: error waitUntilDone: NO];
}


@end
