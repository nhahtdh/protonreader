//
//  RateView.h
//  CustomView
//
//  Created by Shubham on __/__/____.
//  Copyright (c) 2012 BatmanProductions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RateView;
@class RatingWidgetViewController;



@interface RateView : UIView

@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *halfSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;
@property (assign, nonatomic) float rating;
@property (assign) BOOL editable;
@property (strong) NSMutableArray * imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (weak) RatingWidgetViewController* controller;

- (id)initWithFrame:(CGRect)frame Controller:(RatingWidgetViewController*)viewController InitialRating:(float)previousRating;

@end
