//
//  ResourceLoader.h
//  ProtonReader
//
//  Created by  on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Resource.h"
#import "ResourceObserver.h"

// This class will manage all explicit requests made in the program.
// Requests can be sent anonymously (i.e. nil delegate) to pre-load data.

@interface ResourceLoader : NSObject <NSURLConnectionDataDelegate> {
    NSMutableDictionary* runningRequests;
    // NOTE: A finished request will be removed from the list right away
}

+ (void) asyncLoadResource: (NSURLRequest*) aRequest delegate: (id) aDelegate completionHandler: (SEL) aCompletionHandler errorHandler: (SEL) aErrorHandler;

@end
