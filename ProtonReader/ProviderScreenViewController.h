//
//  ProviderScreenViewController.h
//  ProtonReader
//
//  Created by  on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PublisherModel.h"
#import "ThemeModel.h"

@interface ProviderScreenViewController : UIViewController {
    NSOperationQueue *queue;
    
    PublisherModel *publisherModel;
    ThemeModel *themeModel;
    NSMutableArray *articleModels;
    NSMutableArray *articleIcons;
    
    // BOOL requestLock;
    NSMutableData *receivedData;
    NSMutableData *receivedThemeData;
}

@property (strong, nonatomic) PublisherModel *publisherModel;
@property (strong, nonatomic) ThemeModel *themeModel;
@property (strong, nonatomic) NSMutableArray *articleModels;

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIImageView *headerView;

@property (weak, nonatomic) IBOutlet UIScrollView *articleListView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
 
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIButton *subscribeButton;
@property (weak, nonatomic) IBOutlet UITextView *summaryView;


#pragma mark - Event Handler

- (IBAction)handleBack:(UIButton *)sender;

- (IBAction)handleSubscribe:(UIButton *)sender;

@end
