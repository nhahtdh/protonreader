//
//  Resource.m
//  ProtonReader
//
//  Created by  on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Resource.h"

@interface Resource ()

@property (strong, atomic) NSMutableArray* observers;

@end

@implementation Resource

@synthesize data;
@synthesize error;
@synthesize observers;
// @synthesize isLoading;

#pragma mark - Initialization

- (id) init {
    if (self = [super init]) {
        observers = [NSMutableArray array];
    }
    
    return self;
}

#pragma mark - Observer

- (void) addObserver:(ResourceObserver *)aObserver {
    if (aObserver)
        [self.observers addObject: aObserver];
}

- (void) notifyCompletion {
    NSData* immutableData = [NSData dataWithData: self.data];
    
    for (ResourceObserver* observer in self.observers) {
        [observer performCompletionHandlerWithData: immutableData];
    }
}

- (void) notifyError {
    for (ResourceObserver* observer in self.observers) {
        [observer performErrorHandlerWithError: self.error];
    }
}

@end
