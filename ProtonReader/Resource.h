//
//  Resource.h
//  ProtonReader
//
//  Created by  on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResourceObserver.h"

@interface Resource : NSObject {
    NSData *data;
    NSError *error;
    // BOOL isLoading;
    
    NSMutableArray* observers;
}

#pragma mark - Properties

@property (strong, nonatomic) NSData* data;
@property (strong, nonatomic) NSError* error;
// @property (nonatomic) BOOL isLoading;

// After the resource finishes loading, if an error happened, then error != nil

#pragma mark - Observers

- (void) addObserver: (ResourceObserver*) aObserver;

- (void) notifyCompletion;

- (void) notifyError;

@end