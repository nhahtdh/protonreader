//
//  RatingWidgetViewController.m
//  RenderTextDemo
//
//  Created by Shubham Goyal on 27/3/12.
//  Copyright (c) 2012 shubham.goyal@live.in. All rights reserved.
//

#import "RatingWidgetViewController.h"

#define RATING_WIDGET_HEIGHT 50

@implementation RatingWidgetViewController

@synthesize rateView;
@synthesize mainViewController;
@synthesize rating;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithMainController:(ArticleViewController*)controller {
    if(self = [super init]) {
        self.mainViewController = controller;   
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.rateView = [[RateView alloc] initWithFrame:CGRectMake(412, self.mainViewController.view.frame.size.height/* - self.mainViewController.bar.frame.size.height*/, 200, RATING_WIDGET_HEIGHT) Controller:self InitialRating:self.rating];
    
    [self.mainViewController.view addSubview:self.rateView];
    
    CGRect newFrame = CGRectMake(self.rateView.frame.origin.x, /*self.rateView.frame.origin.y - 200*/self.mainViewController.view.frame.size.height - self.mainViewController.bar.frame.size.height, self.rateView.frame.size.width, self.rateView.frame.size.height);
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.rateView.frame = newFrame;
                     }];
    
    self.rateView.notSelectedImage = [UIImage imageNamed:@"kermit_empty.png"];
    self.rateView.halfSelectedImage = [UIImage imageNamed:@"kermit_half.png"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"kermit_full.png"];
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)viewDidUnload
{   
    CGRect newFrame = CGRectMake(self.rateView.frame.origin.x, self.mainViewController.view.frame.size.height/*self.mainViewController.view.frame.size.width - self.mainViewController.bar.frame.size.height - RATING_WIDGET_HEIGHT*/, self.rateView.frame.size.width, self.rateView.frame.size.height);
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.rateView.frame = newFrame;
                     }
                     completion:^(BOOL finished){
                         [self.rateView removeFromSuperview];
                         self.rateView = nil;
                     }];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);

}

@end