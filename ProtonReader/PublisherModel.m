//
//  PublisherModel.m
//  ProtonReader
//
//  Created by  on 3/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PublisherModel.h"

@implementation PublisherModel

@synthesize publisherId;
@synthesize name;
@synthesize logo;
@synthesize summary;

#pragma mark - Custom initialization

#define kPublisherIdKey @"publisherId"
#define kNameKey @"name"
#define kLogo @"logo"
#define kSummary @"summary"

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        publisherId = [aDecoder decodeIntegerForKey: kPublisherIdKey];
        name = [aDecoder decodeObjectForKey: kNameKey];
        logo = [aDecoder decodeObjectForKey: kLogo];
        summary = [aDecoder decodeObjectForKey: kSummary];
    }
    
    return self;
}

#define kJSONPublisherIdKey @"id"
#define kJSONNameKey @"name"
#define kJSONLogoKey @"logo"
#define kJSONSummaryKey @"summary"

#define kDefaultName @"(No name)"
#define kDefaultSummary @"(No summary)"

- (id) initWithDictionary:(NSDictionary *)aDict {
    if (self = [super init]) {
        id value = [aDict valueForKey: kJSONPublisherIdKey];
        assert(value && [value isKindOfClass: [NSNumber class]]);
        publisherId = [(NSNumber*) value unsignedIntegerValue];
                         
        value = [aDict valueForKey: kJSONNameKey];
        if (value && [value isKindOfClass: [NSString class]]) {
            name = value;
        } else {
            name = kDefaultName;
        }
        
        NSURL *encodedImage = [[NSURL alloc] initWithString: [aDict valueForKey: kJSONLogoKey]];
        logo = [[NSData alloc] initWithContentsOfURL: encodedImage];
        
        value = [aDict valueForKey: kJSONSummaryKey];
        if (value && [value isKindOfClass: [NSString class]]) {
            summary = value;
        } else {
            summary = kDefaultSummary;
        }
    }
    
    return self;
}

#pragma mark - Persistence

- (void) encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger: self.publisherId forKey: kPublisherIdKey];
    [aCoder encodeObject: self.name forKey: kNameKey];
    [aCoder encodeObject: self.logo forKey: kLogo];
    [aCoder encodeObject: self.summary forKey: kSummary];
}

#pragma mark - Object identity

- (BOOL) isEqual:(id)object {
    if ([object isKindOfClass: [PublisherModel class]]) {
        return self.publisherId == ((PublisherModel*) object).publisherId;
    }
    
    return NO;
}

- (NSUInteger) hash {
    return self.publisherId;
}

@end
