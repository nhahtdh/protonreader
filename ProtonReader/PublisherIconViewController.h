//
//  PublisherIconViewController.h
//  ProtonReader
//
//  Created by  on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PublisherModel.h"

@interface PublisherIconViewController : UIViewController {
    PublisherModel* model;
}

#pragma mark - Instance properties

@property (strong, nonatomic) PublisherModel* model;

#pragma mark - View elements

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *name;

// - (void) updateView;

@end
