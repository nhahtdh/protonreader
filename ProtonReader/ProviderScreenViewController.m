//
//  ProviderScreenViewController.m
//  ProtonReader
//
//  Created by  on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProviderScreenViewController.h"
#import "ArticleIconViewController.h"
#import "ArticleModel.h"
#import "SubscriptionManager.h"
#import "SDURLCache.h"

@interface ProviderScreenViewController ()

@property (strong, nonatomic) NSMutableArray* articleIcons;

- (void) loadData;
- (void) loadArticleList;
- (void) parseArticleList: (NSData*) data;

// - (void) loadTheme;
// - (void) updateView;

@end

@implementation ProviderScreenViewController

#define recentArticleURLFormat @"http://ec2-175-41-180-218.ap-southeast-1.compute.amazonaws.com/article/recent_article/%u"
#define themeURLFormat @"http://ec2-175-41-180-218.ap-southeast-1.compute.amazonaws.com/publisher/get_theme/%u"

@synthesize publisherModel;
@synthesize themeModel;
@synthesize articleModels;
@synthesize articleIcons;

@synthesize headerView;
@synthesize articleListView;
@synthesize activityIndicator;
@synthesize iconView;
@synthesize subscribeButton;
@synthesize summaryView;

#pragma mark - Custom initialization

- (id) initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder: aDecoder]) {
        queue = [[NSOperationQueue alloc] init];
        articleModels = [NSMutableArray array];
        articleIcons = [NSMutableArray array];
        
        // requestLock = NO;
    }
    
    return self;
}

#pragma mark - Network and data

// NOTE: load___ function will load and update view accordingly
// NOTE: update___ function will take the existing data and update the view

- (void) loadData {
    [self loadArticleList];
    
    // [self updateView];
}

- (BOOL) shouldRetry: (NSError*) error {
    // RETURNS: YES if we should retry. NO if the error is fatal.
    switch (error.code) {
        case NSURLErrorBadURL:
        case NSURLErrorBadServerResponse:
        case NSURLErrorRedirectToNonExistentLocation:
        case NSURLErrorFileDoesNotExist:
        case NSURLErrorFileIsDirectory:
        case NSURLErrorUnsupportedURL:
            DLog(@"Fatal error. Error code: %d. Debug description: %@", error.code, error.debugDescription);
            return NO;
            
        default:
            DLog(@"Error occurred. Error code: %d. Debug description: %@", error.code, error.debugDescription);
            return YES;
    }
}

#define kJSONSuccessKey @"success"
#define kJSONErrorMessageKey @"error_msg"
#define kJSONArticleListKey @"data"

- (void) parseArticleList:(NSData *)data {
    NSError* jsonParsingError = nil;
    NSDictionary* responseJSON = [NSJSONSerialization JSONObjectWithData: data options: kNilOptions error: &jsonParsingError];
    
    assert(responseJSON == nil ^ jsonParsingError == nil);
    if (jsonParsingError) {
        DLog(@"JSON parsing error: %@", jsonParsingError.debugDescription);
        DLog(@"Data dump: %@", [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]);
        return;
    }
    assert([responseJSON isKindOfClass: [NSDictionary class]]);
    
    BOOL isSuccess = [(NSNumber*) [responseJSON valueForKey: kJSONSuccessKey] boolValue];
    if (!isSuccess) {
        DLog(@"Error encountered on server: %@", [responseJSON valueForKey: kJSONErrorMessageKey]);
        DLog(@"Data dump: %@",  [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]);
        return;
    }
    
    NSArray* articleList = [responseJSON valueForKey: kJSONArticleListKey];
    assert(articleList && [articleList isKindOfClass: [NSArray class]]);
    DLog(@"Number of articles in JSON: %d", articleList.count);
    [self.articleModels removeAllObjects];
    for (NSDictionary* article in articleList) {
        ArticleModel *model = [[ArticleModel alloc] initWithDictionary: article];
        [self.articleModels addObject: model];
    }
    DLog(@"Number of article models: %d", self.articleModels.count);
}

#define kJSONThemeKey @"data"

- (void) parseTheme:(NSData *)data {
    NSError* jsonParsingError = nil;
    NSDictionary* responseJSON = [NSJSONSerialization JSONObjectWithData: data options: kNilOptions error: &jsonParsingError];
    
    assert(responseJSON == nil ^ jsonParsingError == nil);
    if (jsonParsingError) {
        DLog(@"JSON parsing error: %@", jsonParsingError.debugDescription);
        DLog(@"Data dump: %@", [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]);
        return;
    }
    assert([responseJSON isKindOfClass: [NSDictionary class]]);
    
    BOOL isSuccess = [(NSNumber*) [responseJSON valueForKey: kJSONSuccessKey] boolValue];
    if (!isSuccess) {
        DLog(@"Error encountered on server: %@", [responseJSON valueForKey: kJSONErrorMessageKey]);
        DLog(@"Data dump: %@",  [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding]);
        return;
    }
    
    /*
    NSArray* articleList = [responseJSON valueForKey: kJSONArticleListKey];
    assert(articleList && [articleList isKindOfClass: [NSArray class]]);
    DLog(@"Number of articles in JSON: %d", articleList.count);
    [self.articleModels removeAllObjects];
    for (NSDictionary* article in articleList) {
        ArticleModel *model = [[ArticleModel alloc] initWithDictionary: article];
        [self.articleModels addObject: model];
    }
    DLog(@"Number of article models: %d", self.articleModels.count);
     */
    
    NSDictionary* themeImages = [responseJSON valueForKey: kJSONThemeKey];
    assert(themeImages && [themeImages isKindOfClass: [NSDictionary class]]);
    self.themeModel = [[ThemeModel alloc] initWithDictionary: themeImages];
}


// TODO: Is there a good way to obtain the value of icon height
#define ICON_HEIGHT 150
#define Y_SEPARATION 5

- (void) updateArticleListView {
    for (ArticleIconViewController *icon in articleIcons) {
        [icon.view removeFromSuperview];
        [icon removeFromParentViewController];
    }
    [self.articleIcons removeAllObjects];
    
    CGPoint origin = CGPointMake(0, 0);
    
    self.articleListView.contentSize = CGSizeMake(self.articleListView.bounds.size.width, (ICON_HEIGHT + Y_SEPARATION) * self.articleModels.count - Y_SEPARATION);
    
    for (ArticleModel* model in self.articleModels) {
        ArticleIconViewController *icon = [[UIStoryboard storyboardWithName: @"MainStoryboard" bundle: nil] instantiateViewControllerWithIdentifier:@"ArticleIconView"];
        icon.model = model;
        
        [self addChildViewController: icon];
        [self.articleIcons addObject: icon];
        [self.articleListView addSubview: icon.view];
        
        icon.view.frame = CGRectMake(origin.x, origin.y, icon.view.frame.size.width, icon.view.frame.size.height);
        
        origin.y += ICON_HEIGHT + Y_SEPARATION;
    }
}

/*
- (void) updateView {
    [self updateArticleListView];
    
}*/

- (BOOL) isFatalError: (NSError*) error {
    switch (error.code) {
        case NSURLErrorBadURL:
        case NSURLErrorBadServerResponse:
        case NSURLErrorRedirectToNonExistentLocation:
        case NSURLErrorFileDoesNotExist:
        case NSURLErrorFileIsDirectory:
        case NSURLErrorUnsupportedURL:
            DLog(@"Fatal error. Error code: %d. Debug description: %@", error.code, error.debugDescription);
            return YES;
            
        default:
            DLog(@"Error occurred. Error code: %d. Debug description: %@", error.code, error.debugDescription);
            return NO;
    }
}

- (void) loadArticleList { // loadData
    // TODO: Refactor the code. Too many repetitive parts in the network code across classes.
    
    // NOTE: This function is not thread safe. Never call this function from different threads.
    
    // TODO: Clear off the view, or set a timer to not reset the view.
    // self.articleListView.hidden = YES;
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    
    NSURL* recentArticleURL = [[NSURL alloc] initWithString: [NSString stringWithFormat: recentArticleURLFormat , self.publisherModel.publisherId]];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL: recentArticleURL 
                                                  cachePolicy: NSURLRequestReloadRevalidatingCacheData 
                                              timeoutInterval: 60.];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (theConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        receivedData = [NSMutableData data];
    } else {
        // Inform the user that the connection failed.
    }
    
    NSURL* themeURL = [[NSURL alloc] initWithString: [NSString stringWithFormat: themeURLFormat, self.publisherModel.publisherId]];
    NSURLRequest *themeRequest = [[NSURLRequest alloc] initWithURL: themeURL cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval: 60.];
    
    NSURLConnection *themeConnection = [[NSURLConnection alloc] initWithRequest: themeRequest delegate:self];
    if (themeConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        receivedThemeData = [NSMutableData data];
    } else {
        // Inform the user that the connection failed.
    }
}

- (void) updateHeader {
    self.headerView.image = self.themeModel.primaryHeader;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.iconView.image = [[UIImage alloc] initWithData: publisherModel.logo];
    // self.iconView.layer.cornerRadius = 10;
    if (self.iconView.image) {
        self.iconView.backgroundColor = [UIColor clearColor];
    } else {
        self.iconView.backgroundColor = [UIColor whiteColor];
    }
    
    self.summaryView.text = self.publisherModel.summary;
    if ([[SubscriptionManager getInstance] isSubscribedTo: self.publisherModel]) {
        [self.subscribeButton setTitle: @"UNSUBSCRIBE" forState: UIControlStateNormal];
        [self.subscribeButton setTitle: @"UNSUBSCRIBE" forState: UIControlStateHighlighted];
        [self.subscribeButton setTitleColor:[UIColor colorWithRed: 1. green: 0.0625 blue: 0.125 alpha: 1] forState: UIControlStateNormal];
        [self.subscribeButton setTitleColor:[UIColor colorWithRed: 1. green: 0.0625 blue: 0.125 alpha: 1] forState: UIControlStateHighlighted];
    } else {
        [self.subscribeButton setTitle: @"SUBSCRIBE" forState: UIControlStateNormal];
        [self.subscribeButton setTitle: @"SUBSCRIBE" forState: UIControlStateHighlighted];
        [self.subscribeButton setTitleColor: [UIColor colorWithRed:0 green:0.625 blue: 0.0625 alpha:1] forState: UIControlStateNormal];
        [self.subscribeButton setTitleColor: [UIColor colorWithRed:0 green:0.625 blue: 0.0625 alpha:1] forState: UIControlStateHighlighted];
    }
    
    [self loadArticleList];
    // [self loadData];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setIconView:nil];
    [self setSummaryView:nil];
    [self setHeaderView:nil];
    [self setArticleListView:nil];
    [self setActivityIndicator:nil];
    [self setSubscribeButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	// return YES;
    // TODO: May add support for vertical orientation
    return interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft;
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    if ([[connection.originalRequest.URL.absoluteString stringByDeletingLastPathComponent]
         isEqual: [themeURLFormat stringByDeletingLastPathComponent]]) {
        DLog(@"Theme");
        [receivedThemeData setLength: 0];
    } else if ([[connection.originalRequest.URL.absoluteString stringByDeletingLastPathComponent]
                isEqual: [recentArticleURLFormat stringByDeletingLastPathComponent]]) {
        DLog(@"Article");
        [receivedData setLength: 0];
    } else {
        DLog(@"BAD");
        assert(0);
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    
    if ([[connection.originalRequest.URL.absoluteString stringByDeletingLastPathComponent]
         isEqual: [themeURLFormat stringByDeletingLastPathComponent]]) {
        [receivedThemeData appendData: data];
    } else if ([[connection.originalRequest.URL.absoluteString stringByDeletingLastPathComponent]
                isEqual: [recentArticleURLFormat stringByDeletingLastPathComponent]]) {
        [receivedData appendData: data];
    } else {
        assert(0);
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DLog(@"Connection failed! Error - %@ %@ %@", connection.originalRequest,
         [error localizedDescription],
         [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    
    if ([[connection.originalRequest.URL.absoluteString stringByDeletingLastPathComponent]
         isEqual: [themeURLFormat stringByDeletingLastPathComponent]]) {
        [self parseTheme: receivedThemeData];
        [self updateHeader];
    } else if ([[connection.originalRequest.URL.absoluteString stringByDeletingLastPathComponent]
                isEqual: [recentArticleURLFormat stringByDeletingLastPathComponent]]) {
        [self parseArticleList:receivedData];
        // activityIndicator.hidden = YES;
        [self updateArticleListView];
    }
}

#pragma mark - Handle action

- (IBAction)handleBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)handleSubscribe:(UIButton *)sender {
    SubscriptionManager *subscriptionManager = [SubscriptionManager getInstance];
    if ([subscriptionManager isSubscribedTo: self.publisherModel]) {
        DLog(@"Susbcribed - Now unsubscribing");
        [subscriptionManager unsubscribeToPublisher: self.publisherModel];
        // [UIView beginAnimations: @"Unsubscribe" context:nil];
        // [UIView setAnimationDuration: 0.2];
        [self.subscribeButton setTitle: @"SUBSCRIBE" forState: UIControlStateNormal];
        [self.subscribeButton setTitle: @"SUBSCRIBE" forState: UIControlStateHighlighted];
        [self.subscribeButton setTitleColor: [UIColor colorWithRed:0 green:0.625 blue: 0.0625 alpha:1] forState: UIControlStateNormal];
        [self.subscribeButton setTitleColor: [UIColor colorWithRed:0 green:0.625 blue: 0.0625 alpha:1] forState: UIControlStateHighlighted];
        // [UIView commitAnimations];
    } else {
        DLog(@"Unsubscribed - Now subscribing");
        [subscriptionManager subscribeToPublisher: self.publisherModel];
        // [UIView beginAnimations: @"Subscribe" context:nil];
        // [UIView setAnimationDuration: 0.2];
        [self.subscribeButton setTitle: @"UNSUBSCRIBE" forState: UIControlStateNormal];
        [self.subscribeButton setTitle: @"UNSUBSCRIBE" forState: UIControlStateHighlighted];
        [self.subscribeButton setTitleColor:[UIColor colorWithRed: 1. green: 0.0625 blue: 0.125 alpha: 1] forState: UIControlStateNormal];
        [self.subscribeButton setTitleColor:[UIColor colorWithRed: 1. green: 0.0625 blue: 0.125 alpha: 1] forState: UIControlStateHighlighted];
        // [UIView commitAnimations];
    }
}

@end
