//
//  ThemeModel.h
//  ProtonReader
//
//  Created by  on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThemeModel : NSObject {
    // NSString *logo;
    UIImage* primaryHeader;
    UIImage* secondaryHeader;
    // NSString *primaryHeader;
    // NSString *secondaryHeader;
}

@property (strong, nonatomic) UIImage* primaryHeader;
@property (strong, nonatomic) UIImage* secondaryHeader;

- (id) initWithDictionary: (NSDictionary*) aDict;


@end
